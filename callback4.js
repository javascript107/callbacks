let boardDetails = require("./callback2");
let boardsData = require("./data/boards.json");
let listsData = require("./data/lists.json");
let boardInfo = require("./callback1");
let cardsData = require("./data/cards.json");
let cardsDetails = require("./callback3");

const requiredBoardData = function (boardName, listName) {
  setTimeout(() => {
    // Your code here
    let result = [];
    let boardId, listId;
    for (let board of boardsData) {
      if (board["name"] === boardName) {
        result.push(
          boardInfo(board["id"], boardsData),
          boardDetails(board["id"], listsData)
        );
      }
    }
    let listInfo =  Object.entries(listsData).map(each=>[...each[1]]).flat()
    const card_id = listInfo.reduce((acc,curr)=>{ 
      if (curr.name === "Mind"){
          acc.push(curr.id)
      }
      return acc
    },[])
     result.push(cardsDetails(card_id,cardsData))

    return result
  }, 2 * 1000);
};

module.exports = requiredBoardData;
