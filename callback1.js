/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/
const boardInfo = function (boardID, data) {

     setTimeout(() => {
      // Your code here
      if(!boardID || !data){
        reject('invalid data');
      }
      let res = data.filter((item) => {
        return item.id == boardID;
      });
      console.log(res)
    }, 2 * 1000);
  
};

module.exports = boardInfo;
