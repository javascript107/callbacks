/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

const cardDetails = function(cardID,data) {
        setTimeout(() => {
            // Your code here
            for(let card of Object.keys(data)){
                if (card == cardID){
                    console.log(data[card])
                }
            }
    }, 2 * 1000);
}

module.exports = cardDetails