/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

const boardDetails = function (listID, data) {

    setTimeout(() => {
      // Your code here
      for (let board of Object.keys(data)) {
        if (board == listID) {
          console.log(data[board]);
        }
      }
    }, 2 * 1000);
  };


module.exports = boardDetails;
